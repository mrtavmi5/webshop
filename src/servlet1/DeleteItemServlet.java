package servlet1;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.webshop.ShoppingCart;
import servlet1.webshop.User;

/**
 * Servlet implementation class DeleteItemServlet
 */
public class DeleteItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		User logovani = (User) session.getAttribute("Ulogovani user");

		if (logovani == null) {
			response.sendRedirect("Login.html");
			return;
		}
		
		ShoppingCart sc = logovani.getShoppingCart();
		
		sc.removeItem(Integer.parseInt(request.getParameter("itemNumber")));
		
		response.sendRedirect("ShoppingCartServlet");
	}

}
