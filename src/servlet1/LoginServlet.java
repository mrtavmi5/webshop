package servlet1;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.webshop.User;
import servlet1.webshop.Users;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
	}

	@Override
	public void init(ServletConfig cfg) {
		try {
			super.init(cfg);
		} catch (ServletException e) {
			e.printStackTrace();
		}
		ServletContext ctx = getServletContext();
		if (ctx.getAttribute("users") == null) {
			Users users = new Users(ctx.getRealPath(""));
			ctx.setAttribute("users", users);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Users users = (Users) getServletContext().getAttribute("users");
		HttpSession session = request.getSession();
		User logovani = (User) session.getAttribute("Ulogovani user");

		if (logovani != null) {
			response.sendRedirect("WebShopServlet");
			return;
		}

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		User user = users.getUsers().get(username);

		if (user != null && user.getPassword().equals(password)) {
			session.setAttribute("Ulogovani user", user);

			response.sendRedirect("WebShopServlet");
		} else {
			response.sendRedirect("Login.html");
		}
	}

}
