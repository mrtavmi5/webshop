package servlet1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.webshop.User;
import servlet1.webshop.Users;

/**
 * Servlet implementation class UsersListServlet
 */
public class UsersListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User logovani = (User) session.getAttribute("Ulogovani user");

		if (logovani == null) {
			response.sendRedirect("Login.html");
			return;
		}

		response.setContentType("text/html");

		PrintWriter pout = response.getWriter();

		pout.println("<html>");
		pout.println("<head>");
		pout.println("</head>");
		pout.println("<body>");
		pout.println("<table border = 1>");
		pout.println("<tr><th>Username</th><th>Password</th></tr>");
		Users users = (Users) getServletContext().getAttribute("users");
		for (User u : users.getUsers().values()) {
			pout.println("<tr>");
			pout.print("<td>" + u.getUsername() + "</td>");
			pout.println("<td>" + u.getPassword() + "</td>");
			pout.println("</tr>");
		}
		pout.println("</table>");
		
		pout.println("<p>");
		pout.println("<a href=\"WebShopServlet\">Povratak</a>");
		pout.println("</p>");

		
		pout.println("</body>");
		pout.println("</html>");
	}

}
