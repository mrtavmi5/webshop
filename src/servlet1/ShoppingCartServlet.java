package servlet1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.webshop.Product;
import servlet1.webshop.Products;
import servlet1.webshop.ShoppingCart;
import servlet1.webshop.ShoppingCartItem;
import servlet1.webshop.User;

/**
 * Klasa koja obavlja listanje stavki u korpi, a ako je pozvana iz forme, dodaje
 * jednu stavku u korpu, pa onda lista).
 */
public class ShoppingCartServlet extends HttpServlet {

	private static final long serialVersionUID = -8628509500586512294L;

	// zasto ovako ne valja?
	// obratiti paznju na prirodu http protokola, koji je stateless
	// private ShoppingCart sc = new ShoppingCart();

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// zasto ovako ne valja?
		// obratiti paznju na prirodu http protokola, koji je stateless
		// ShoppingCart sc = new ShoppingCart();

		// pogledamo da li u tekucoj sesiji postoji objekat ShoppingCart
		HttpSession session = request.getSession();
		User logovani = (User) session.getAttribute("Ulogovani user");

		if (logovani == null) {
			response.sendRedirect("Login.html");
			return;
		}
		ShoppingCart sc = logovani.getShoppingCart();
		response.setContentType("text/html");

		PrintWriter pout = response.getWriter();

		pout.println("<html>");
		pout.println("<head>");
		pout.println("</head>");
		pout.println("<body>");
		pout.println("Proizvodi u korpi:");
		pout.println(
				"<table><tr bgcolor=\"lightgrey\"><th>Naziv</th><th>Jedinicna cena</th><th>Komada</th><th>Ukupna cena</th><th>Obrisi</th></tr>");
		double total = 0;
		int i = 0;
		for (ShoppingCartItem item : sc.getItems()) {
			pout.println("<tr>");
			pout.println("<form method=\"post\" action=\"DeleteItemServlet\">");
			pout.println("<td>" + item.getProduct().getName() + "</td>");
			pout.println("<td>" + item.getProduct().getPrice() + "</td>");
			pout.println("<td>" + item.getCount() + "</td>");
			double price = item.getProduct().getPrice() * item.getCount();
			pout.println("<td>" + price + "</td>");
			pout.println("<input type=\"hidden\" name=\"itemNumber\" value=\"" + i + "\">");
			pout.println("<td><input type=\"submit\" value=\"X\" /></td>");
			pout.println("</form>");
			pout.println("</tr>");
			total += price;
			i++;
		}
		pout.println("</table>");

		pout.println("<p>");
		pout.println("Ukupno: " + total + " dinara.");
		pout.println("</p>");

		pout.println("<p>");
		pout.println("<a href=\"WebShopServlet\">Povratak</a>");
		pout.println("</p>");

		pout.println("</body>");
		pout.println("</html>");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		User logovani = (User) session.getAttribute("Ulogovani user");

		if (logovani == null) {
			response.sendRedirect("Login.html");
			return;
		}
		ShoppingCart sc = logovani.getShoppingCart();

		if (request.getParameter("itemId") != null) {
			// ako smo pozvali ovaj servlet sa parametrima za dodavanje proizvoda u korpu
			try {
				Products products = (Products) getServletContext().getAttribute("products");
				Product product = products.getProduct(request.getParameter("itemId"));
				int itemCount = Integer.parseInt(request.getParameter("itemCount"));
				if (itemCount > 0) {
					sc.addItem(product, itemCount);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		doGet(request, response);

	}
}
